//
//  SwPerson.swift
//  webrequest-nsurl
//
//  Created by iulian david on 11/14/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import Foundation

class SWPerson{
    
    private var _name: String!
    private var _height: String!
    private var _birth: String!
    private var _hairColor: String!
    
    init(name: String, height: String, birth:String, hairColor: String) {
        _name = name;
        _height = height
        _birth = birth
        _hairColor = hairColor
    }
    
    var name: String {
        get{
            return _name
        }
    }
    
    var height: String {
        get{
            return _height
        }
    }
    
    
    var birth: String {
        get{
            return _birth
        }
    }
    
    
    var hairColor: String {
        get{
            return _hairColor
        }
    }
}
