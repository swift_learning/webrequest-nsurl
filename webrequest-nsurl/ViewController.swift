//
//  ViewController.swift
//  webrequest-nsurl
//
//  Created by iulian david on 11/14/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit

/**
 Usage of URLRequest (formely known as NSUrlSession)
 Parsing of JSON content
 
 CODE for parsing:
 ```
 
 session.dataTask(with: URLRequest(url: url), completionHandler:{data, response, error
 in
 if let responseData = data {
 do {
 let json = try JSONSerialization.jsonObject(with: responseData, options:
 JSONSerialization.ReadingOptions.allowFragments)
 
 if let dict = json as? Dictionary<String,AnyObject> {
 
 print("We got here: \(dict.debugDescription)")
 if let vehicles = dict["vehicles"] as? [String] {
 print("The vehicles: \(vehicles.debugDescription)")
 }
 if let species = dict["species"] as? [String] {
 print("The species: \(species.debugDescription)")
 
 }
 }
 }
 } catch {
 print("cannot serialize")
 }
 }
 
 }).resume()
 ```
 */
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let urlString = "http://swapi.co/api/people/1"
        let session = URLSession.shared
        let url = URL(string: urlString)!
        
        session.dataTask(with: URLRequest(url: url), completionHandler:{data, response, error
            in
            if let responseData = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: responseData, options:
                    JSONSerialization.ReadingOptions.allowFragments)
                    
                    if let dict = json as? Dictionary<String,AnyObject> {

                        
                        if let name = dict["name"] as? String, let height = dict["height"] as? String, let birth = dict["birth_year"] as? String,
                            let hairColor = dict["hair_color"] as? String {
                            let person = SWPerson(name: name, height: height, birth: birth, hairColor: hairColor)
                            print(person.name)
                        }
                    }
                } catch {
                    print("cannot serialize")
                }
            }
            
        }).resume()
        
        
    }

}

